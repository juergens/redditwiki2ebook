
PROJECT_PATH=$(shell realpath .)
PRE_CMD=mkdir -p build
FILES=page.md
OUT=$(PROJECT_PATH)/build/output

BASE_CMD=pandoc --toc --standalone --self-contained --toc -N  --reference-links --reference-location section -V links-as-notes

WEB_CMD=$(BASE_CMD) --webtex --section-divs --reference-links  --metadata title=my_title

.PHONY: clean
default: mobi
.DEFAULT_GOAL := mobi

all:  mobi epub

clean:
	rm -rf build

page.md:
		$(PRE_CMD) && \
		python3 src/main.py

$(OUT).html: $(FILES)
		$(PRE_CMD) && \
		$(WEB_CMD) -o $(OUT).html -t html page.md
html: $(OUT).html

$(OUT).epub: $(FILES)
		$(PRE_CMD) && \
		$(WEB_CMD) -o $(OUT).epub page.md
epub: $(OUT).epub

$(OUT).fb2: $(FILES)
		$(PRE_CMD) && \
		$(WEB_CMD) -o $(OUT).fb2 page.md
fb2: $(OUT).fb2

$(OUT).mobi: $(OUT).fb2
		$(PRE_CMD) && \
		ebook-convert $(OUT).fb2 $(OUT).mobi > /dev/null
mobi: $(OUT).mobi
