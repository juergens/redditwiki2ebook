
import os
import praw

reddit = praw.Reddit(client_id=os.environ['client_id'],
                     client_secret=os.environ['client_secret'],
                     user_agent='ubuntu:de.wotanii.wikidownloader:v0.1 (by /u/wotanii)')
wikipage = reddit.subreddit(os.environ['sub']).wiki[os.environ['page']]
text_file = open("page.md", "wb")
text_file.write(wikipage.content_md.encode('utf-8'))
text_file.close()
