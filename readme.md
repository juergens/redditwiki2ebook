


if you want to download the page `faq` from the wiki of the subreddit `Stoicism` you would do this:

    git clone git@gitlab.com:juergens/redditwiki2ebook.git
    docker build -t epuber . && docker run -e client_id='MY_CLIENT_ID' -e client_secret='MY_CLIENT_SECRET  -e sub='Stoicism' -e page='faq' -v $PWD/build:/work/build -it epuber

This will create a directory called "build" in which the ebooks will appear automagically. 

How to obtain MY_CLIENT_ID and MY_CLIENT_SECRET is explained [here](https://github.com/reddit-archive/reddit/wiki/OAuth2-Quick-Start-Example#first-steps)

