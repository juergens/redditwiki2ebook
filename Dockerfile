from registry.gitlab.com/juergens/markdown-latex-boilerplate

run apt update && apt install -y python3-pip python3-dev

run mkdir work
workdir work
add requirements.txt .
run pip3 install -r requirements.txt
add . .

cmd make all
